package com.android.guoguang.lkmvpapp.net;


import com.android.guoguang.lkmvpapp.base.BaseObjectBean;
import com.android.guoguang.lkmvpapp.bean.LoginBean;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 *
 * @作者 liuke
 * @创建日期 2018/12/19 14:11
 * @Email 1012459343@qq.com
 * Description:请求方法
 */

public interface APIService {

    /**
     * 登陆
     *
     * @param username 账号
     * @param password 密码
     * @return
     */
    @FormUrlEncoded
    @POST("user/login")
    Flowable<BaseObjectBean<LoginBean>> login(@Field("username") String username,
                                              @Field("password") String password);

    /**
     * 登陆
     *
     * @param username 账号
     * @param password 密码
     * @return
     */
    @FormUrlEncoded
    @POST("user/login")
    Observable<BaseObjectBean<LoginBean>> ologin(@Field("username") String username,
                                                 @Field("password") String password);


}
