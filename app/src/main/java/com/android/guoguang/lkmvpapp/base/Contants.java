package com.android.guoguang.lkmvpapp.base;

/**
 * Created by liuke on 2018/12/20.
 */

public class Contants {
    public static final int LOADINGDIALOG = 1;
    public static final int SUCCESSDIALOG = 2;
    public static final int FAILUREDIALOG = 3;
    public static final int PROMPTDIALOG = 4;
}
