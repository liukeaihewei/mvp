package com.android.guoguang.lkmvpapp.mvp.presenter;

import com.android.guoguang.lkmvpapp.base.BaseObjectBean;
import com.android.guoguang.lkmvpapp.bean.LoginBean;
import com.android.guoguang.lkmvpapp.mvp.RxPresenter;
import com.android.guoguang.lkmvpapp.mvp.contract.LoginContract;
import com.android.guoguang.lkmvpapp.mvp.model.LoginModel;
import com.android.guoguang.lkmvpapp.untils.rxuntils.RxScheduler;

import io.reactivex.functions.Consumer;

/**
 * Created by liuke on 2018/12/19.
 * 这是一种写法 因为AutoDispose需要传入带有生命周期性质的对象：activity，fragment，service等，所以请求写在presenter中
 * 因为mview中的方法持有activit对象
 * 另外一种写法 把请求写在model中，在activity中去获取disable去主动消除订阅，防止内存泄漏
 */

public class LoginPresenter extends RxPresenter<LoginContract.view> implements LoginContract.Presenter {
    private LoginContract.Model model = new LoginModel();

    @Override
    public void login(String username, String password) {
        //View是否绑定 如果没有绑定，就不执行网络请求
        if (!isViewAttached()) {
            return;
        }
        mView.showLoading();
        model.login(username, password)
                .compose(RxScheduler.<BaseObjectBean<LoginBean>>Flo_io_main())
                .as(mView.<BaseObjectBean<LoginBean>>bindAutoDispose())
                .subscribe(new Consumer<BaseObjectBean<LoginBean>>() {
                    @Override
                    public void accept(BaseObjectBean<LoginBean> bean) throws Exception {
                        mView.onSuccess(bean);
                        mView.hideLoading();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        mView.onError(throwable);
                        mView.hideLoading();
                    }
                });
    }
}
