package com.android.guoguang.lkmvpapp.untils.commonuntils;

import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

import com.android.guoguang.lkmvpapp.MainApp;

/**
 * Created by lk on 2017/9/7.
 */

public class ToastUtils {
    private static Context context = MainApp.getInstance();
    private static Toast toast;

    public static void show(@StringRes int resId) {
        show(context.getResources().getString(resId));
    }

    public static void show(CharSequence text) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.show();
    }
}
