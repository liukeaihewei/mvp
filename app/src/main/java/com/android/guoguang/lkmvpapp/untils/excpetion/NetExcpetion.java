package com.android.guoguang.lkmvpapp.untils.excpetion;

/**
 * 版权所有：中国农业银行软件开发中心
 * 系统名称：mmspc
 * 创建时间：2018/1/15
 * 作者：刘孟轩
 */

public class NetExcpetion {
    //网络异常错误码
    public static final int UNAUTHORIZED = 401;
    public static final int NOT_FOUND = 404;
    public static final int INTERNAL_SERVER_ERROR = 500;
    public static final int GATEWAY_TIMEOUT = 504;
}
