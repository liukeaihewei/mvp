package com.android.guoguang.lkmvpapp;

import android.app.Activity;
import android.app.Application;
import android.content.res.Resources;

import java.util.HashSet;
import java.util.Set;

/**
 * 版权所有：中国农业银行软件开发中心
 * 系统名称：mmspc
 * 创建时间：2018/1/15
 * 作者：刘孟轩
 */

public class MainApp extends Application {
    private static MainApp instance;
    private Set<Activity> allActivities;
    public static synchronized MainApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

    }

    public static Resources getAppResources() {
        return instance.getResources();
    }

    /**
     * 将每一个Activity 添加到App类内部维护的一个集合中  用于退出App使用
     *
     * @param act
     */
    public void addActivity(Activity act) {
        if (allActivities == null) {
            allActivities = new HashSet<>();
        }
        allActivities.add(act);
    }

    /**
     * 删除掉某一个Actiity的方法
     *
     * @param act
     */
    public void removeActivity(Activity act) {
        if (allActivities != null) {
            allActivities.remove(act);
        }

    }

    /**
     * 退出App的方法
     */
    public void exitApp() {
        if (allActivities != null) {
            synchronized (allActivities) {
                for (Activity act : allActivities) {
                    act.finish();
                }
            }
        }

        /**
         * 绕过Activity 生命周期  强制关闭
         */
        //android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }


    @Override
    public void onTerminate() {
        super.onTerminate();

    }

    @Override
    public void onTrimMemory(int level) {
        // 程序在内存清理的时候执行

        super.onTrimMemory(level);
    }


}