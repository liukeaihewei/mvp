package com.android.guoguang.lkmvpapp.mvp;

import android.util.Log;

import com.android.guoguang.lkmvpapp.base.BasePresenter;
import com.android.guoguang.lkmvpapp.base.BaseView;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * Created by sjl on 2017/5/2.
 * 基于Rx的Presenter封装 更好的控制订阅的声明周期
 */

public class RxPresenter<T extends BaseView> implements BasePresenter<T> {
    protected T mView;
    protected CompositeDisposable mCompositeDisposable;


    /**
     * 这些都不用了 可以 这是手动的方法 在用到rxjava异步的时候mCompositeDisposable.add(deleteAppsInfoDisposable);就行
     * RxJava 取消订阅的方法
     */
    protected void unSubscribe() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
        Log.d("TAG", "停止所有的请求");

    }

    public void addSubscribe(Disposable disposable) {
        if (mCompositeDisposable == null) {
            mCompositeDisposable = new CompositeDisposable();
        }
        Log.d("TAG", "请求入列");
        mCompositeDisposable.add(disposable);
    }

    @Override
    public void attachView(T view) {
        this.mView = view;

    }

    @Override
    public void detachView() {

        /**
         * 取消订阅关系的方法  将持有的Activity引用置为空
         */
        this.mView = null;
        unSubscribe();
    }

    @Override
    public boolean isViewAttached() {
        return mView != null;
    }
}
