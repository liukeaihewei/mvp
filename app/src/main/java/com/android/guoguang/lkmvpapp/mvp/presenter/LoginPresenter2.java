package com.android.guoguang.lkmvpapp.mvp.presenter;

import com.android.guoguang.lkmvpapp.base.BaseObjectBean;
import com.android.guoguang.lkmvpapp.bean.LoginBean;
import com.android.guoguang.lkmvpapp.mvp.RxPresenter;
import com.android.guoguang.lkmvpapp.mvp.contract.LoginContract;
import com.android.guoguang.lkmvpapp.mvp.model.LoginModel2;
import com.android.guoguang.lkmvpapp.net.OnNetRequestListener;

/**
 * Created by liuke on 2018/12/19.
 */

public class LoginPresenter2 extends RxPresenter<LoginContract.view> implements LoginContract.Presenter {
    private LoginContract.Model2 model = new LoginModel2(this);

    @Override
    public void login(String username, String password) {
        //View是否绑定 如果没有绑定，就不执行网络请求
        if (!isViewAttached()) {
            return;
        }
        model.login(username, password, new OnNetRequestListener<BaseObjectBean<LoginBean>>() {
            @Override
            public void onStart() {
                mView.showLoading();
            }

            @Override
            public void onFinish() {
                mView.hideLoading();
            }

            @Override
            public void onSuccess(BaseObjectBean<LoginBean> data) {
                mView.onSuccess(data);
            }

            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}
