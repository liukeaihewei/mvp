package com.android.guoguang.lkmvpapp.mvp.contract;

import com.android.guoguang.lkmvpapp.base.BaseObjectBean;
import com.android.guoguang.lkmvpapp.base.BasePresenter;
import com.android.guoguang.lkmvpapp.base.BaseView;
import com.android.guoguang.lkmvpapp.bean.LoginBean;
import com.android.guoguang.lkmvpapp.net.OnNetRequestListener;

import io.reactivex.Flowable;

/**
 *
 * @作者 liuke
 * @创建日期 2018/12/19 14:08
 * @Email 1012459343@qq.com
 * Description:管理器
 */


public interface LoginContract {

    interface Model {
        Flowable<BaseObjectBean<LoginBean>> login(String username, String password);
    }

    interface Model2 {
       void login(String username, String password, OnNetRequestListener<BaseObjectBean<LoginBean>> listener);
    }

    interface view extends BaseView {
        void onSuccess(BaseObjectBean<LoginBean> bean);//这是自己，也可以复写baseview方法
    }

    interface Presenter extends BasePresenter<view>{
        /**
         * 登陆
         *
         * @param username
         * @param password
         */
        void login(String username, String password);
    }
}

