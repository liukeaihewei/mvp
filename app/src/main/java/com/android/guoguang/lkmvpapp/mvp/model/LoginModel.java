package com.android.guoguang.lkmvpapp.mvp.model;


import com.android.guoguang.lkmvpapp.base.BaseObjectBean;
import com.android.guoguang.lkmvpapp.bean.LoginBean;
import com.android.guoguang.lkmvpapp.mvp.contract.LoginContract;
import com.android.guoguang.lkmvpapp.net.RetrofitClient;

import io.reactivex.Flowable;

/**
 *
 * @作者 liuke
 * @创建日期 2018/12/19 14:13
 * @Email 1012459343@qq.com
 * Description:
 */

public class LoginModel implements LoginContract.Model {
    @Override
    public Flowable<BaseObjectBean<LoginBean>> login(String username, String password) {
         return RetrofitClient.getInstance().getApi().login(username,password);
    }
}
