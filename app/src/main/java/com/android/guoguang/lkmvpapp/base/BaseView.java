package com.android.guoguang.lkmvpapp.base;

import com.uber.autodispose.AutoDisposeConverter;

/**
 *
 * @作者 liuke
 * @创建日期 2018/12/19 9:42
 * @Email 1012459343@qq.com
 * Description: 统一的view基类，公用显示方法
 */

public interface BaseView {
  /**
   * 显示加载中
   */
  void showLoading();

  /**
   * 隐藏加载
   */
  void hideLoading();

  /**
   * 数据获取失败
   */
  void showError(String msg);

  /**
   * 数据获取失败
   * @param throwable
   */
  void onError(Throwable throwable);
  /**
   * 提示
   */
  void showToast(String msg);

  /**
   * 显示等待弹窗
   */
  void showWaitingDialog(String tip);

  /**
   * 隐藏等待弹窗
   */
  void hideWaitingDialog();

  /**
   * 绑定Android生命周期 防止RxJava内存泄漏
   *
   * @param <T>
   * @return
   */
  <T> AutoDisposeConverter<T> bindAutoDispose();
}
