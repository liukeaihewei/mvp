package com.android.guoguang.lkmvpapp.untils.commonuntils;

import android.content.Context;

import com.android.guoguang.lkmvpapp.interfaces.Dialog_Inter;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;
import com.qmuiteam.qmui.widget.dialog.QMUITipDialog;

/**
 * 所有工具类的github项目 可以用到什么就拿什么 再结合自己平时用到的
 * https://github.com/Blankj/AndroidUtilCode/blob/master/utilcode/README-CN.md
 * @作者 liuke
 * @创建日期 2018/12/20 14:56
 * @Email 1012459343@qq.com
 * Description: QMUI的dialog 网址https://qmuiteam.com/android/get-started/
 */

public class QMUIDialogUntils {

    private Context context;
    QMUITipDialog tipDialog;
    private int mCurrentDialogStyle = com.qmuiteam.qmui.R.style.QMUI_Dialog;

    public QMUIDialogUntils(Context context) {
        this.context = context;
    }

    /**
     * 黑色背景dialog
     *
     * @param Dialogtype 1正在加载 2操作成功 3操作失败 4提醒
     */
    public void Show_Download_Dialog(int Dialogtype, String text) {
        switch (Dialogtype) {
            case 1:
                tipDialog = new QMUITipDialog.Builder(context)
                        .setIconType(QMUITipDialog.Builder.ICON_TYPE_LOADING)
                        .setTipWord(text)
                        .create();
                tipDialog.show();
                break;
            case 2:
                tipDialog = new QMUITipDialog.Builder(context)
                        .setIconType(QMUITipDialog.Builder.ICON_TYPE_SUCCESS)
                        .setTipWord(text)
                        .create();
                break;
            case 3:
                tipDialog = new QMUITipDialog.Builder(context)
                        .setIconType(QMUITipDialog.Builder.ICON_TYPE_FAIL)
                        .setTipWord(text)
                        .create();
                break;
            case 4:
                tipDialog = new QMUITipDialog.Builder(context)
                        .setIconType(QMUITipDialog.Builder.ICON_TYPE_INFO)
                        .setTipWord(text)
                        .create();
                break;

        }
        tipDialog.show();
    }

    /**
     * 普通dialog 确定 取消类型的
     *
     * @param title
     * @param message
     * @param cancelname
     * @param dialog_inter
     */
    public void showMessagePositiveDialog(String title, String message, String cancelname, final Dialog_Inter dialog_inter) {
        new QMUIDialog.MessageDialogBuilder(context)
                .setTitle(title)
                .setMessage(message)
                .addAction(cancelname, new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog_inter.Cancel();
                        dialog.dismiss();
                    }
                })
                .addAction("确定", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog_inter.Confirm();
                        dialog.dismiss();
                    }
                })
                .create(mCurrentDialogStyle).show();
    }

    /**
     * 提示dialog
     * 接口中cancel不需要具体实现
     *
     * @param title
     * @param message
     */
    private void showLongMessageDialog(String title, String message, String actionname, final Dialog_Inter dialog_inter) {
        new QMUIDialog.MessageDialogBuilder(context)
                .setTitle(title)
                .setMessage(message)
                .addAction(actionname, new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog_inter.Confirm();
                        dialog.dismiss();
                    }
                })
                .create(mCurrentDialogStyle).show();
    }

    /**
     * 关闭dialog
     */
    public void dialogdimss() {
        if (null != tipDialog && tipDialog.isShowing()) {
            tipDialog.dismiss();
        }
    }
}
