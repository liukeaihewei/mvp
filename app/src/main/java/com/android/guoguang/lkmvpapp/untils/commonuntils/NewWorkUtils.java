package com.android.guoguang.lkmvpapp.untils.commonuntils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.android.guoguang.lkmvpapp.MainApp;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by sjl on 2017/1/7.
 */

public class NewWorkUtils {
  /**
   * 检查WIFI是否连接
   */
  public static boolean isWifiConnected() {
    ConnectivityManager connectivityManager = (ConnectivityManager) MainApp.getInstance().getApplicationContext().getSystemService(CONNECTIVITY_SERVICE);
    NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
    return wifiInfo != null;
  }

  /**
   * 检查手机网络(4G/3G/2G)是否连接
   */
  public static boolean isMobileNetworkConnected() {
    ConnectivityManager connectivityManager = (ConnectivityManager) MainApp.getInstance().getApplicationContext().getSystemService(CONNECTIVITY_SERVICE);
    NetworkInfo mobileNetworkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
    return mobileNetworkInfo != null;
  }

  /**
   * 检查是否有可用网络
   */
  public static boolean isNetworkConnected() {
    ConnectivityManager manager = (ConnectivityManager) MainApp.getInstance().getSystemService(CONNECTIVITY_SERVICE);
    // 检查网络连接，如果无网络可用，就不需要进行连网操作等
    NetworkInfo info = manager.getActiveNetworkInfo();
    if (info == null || !manager.getBackgroundDataSetting()) {
      return false;
    }
    return true;
  }
}
