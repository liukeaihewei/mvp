package com.android.guoguang.lkmvpapp.base;

/**
 * Created by sjl on 2017/5/2.
 */

public interface BasePresenter<T extends BaseView> {
  /**
   * 订阅
   */
  void attachView(T view);

  /**
   * 注销
   */
  void detachView();

  /**
   * View是否绑定
   *
   * @return
   */
   boolean isViewAttached();
}
