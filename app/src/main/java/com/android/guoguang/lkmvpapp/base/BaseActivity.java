package com.android.guoguang.lkmvpapp.base;

import android.app.Activity;
import android.arch.lifecycle.Lifecycle;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.android.guoguang.lkmvpapp.MainApp;
import com.android.guoguang.lkmvpapp.mvp.RxPresenter;
import com.uber.autodispose.AutoDispose;
import com.uber.autodispose.AutoDisposeConverter;
import com.uber.autodispose.android.lifecycle.AndroidLifecycleScopeProvider;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;

/**
 *
 * @作者 liuke
 * @创建日期 2018/12/19 10:25
 * @Email 1012459343@qq.com
 * Description: MVP activity的基类
 */

public abstract class BaseActivity<T extends RxPresenter> extends AppCompatActivity implements BaseView {
    protected T mPresenter;
    protected Activity mContext;
    protected FragmentManager mFragmentManager;
    private Unbinder mUnbinder;
    //用于解除订阅关系的防止内存泄漏
    protected CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(getLayout());
        mFragmentManager = getSupportFragmentManager();

        /**
         * 实例化ButterKnife
         */
        mUnbinder = ButterKnife.bind(this);

        /**
         * 上下文
         */
        mContext = this;
        /**
         * 初始化Presenter
         */
        mPresenter = initPresenter();
        /**
         * Presenter 与 Activity 建立订阅关系 让Presenter  持有Activity引用
         */
        if (mPresenter != null) {
            mPresenter.attachView(this);
        }
        /**
         * 将当前Activity 添加到集合中
         */
        MainApp.getInstance().addActivity(this);

        instantiateFragments(savedInstanceState);

        /**
         * 调用初始化数据的方法
         */
        initEventAndData();

    }

    /**
     * 绑定生命周期 防止MVP内存泄漏
     * 在activitydestroy的时候解除
     * @param <T>
     * @return
     */
    @Override
    public <T> AutoDisposeConverter<T> bindAutoDispose() {
        return AutoDispose.autoDisposable(AndroidLifecycleScopeProvider
                .from(this, Lifecycle.Event.ON_DESTROY));
    }

    @CallSuper
    protected void instantiateFragments(Bundle savedInstanceState) {

    }

    /**
     * 设置ToolBar的方法
     *
     * @param toolbar
     * @param title
     */
    protected void setToolBar(Toolbar toolbar, String title) {
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //添加返回按钮
        getSupportActionBar().setDisplayShowHomeEnabled(true);   //是否显示左上角的图标
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    /**
     * 显示等待提示框
     */
/*    public void showWaitingDialog(String tip) {
        hideWaitingDialog();
        View view = View.inflate(this, R.layout.abc_dialog_waiting, null);
        ImageView imageView= (ImageView) view.findViewById(R.id.imageView);
        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getDrawable();
        animationDrawable.start();
        if (!TextUtils.isEmpty(tip))
            ((TextView) view.findViewById(R.id.tvTip)).setText(tip);
        mDialogWaiting = new CustomDialog(this, view, R.style.MyDialog);
        mDialogWaiting.show();
        mDialogWaiting.setCancelable(false);
    }*/

    /**
     * toast提示
     */
    public void showToast(String str) {
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    /**
     * 隐藏等待提示框
     */
/*    public void hideWaitingDialog() {
        if (mDialogWaiting != null) {
            mDialogWaiting.dismiss();
            mDialogWaiting = null;
        }
    }*/

    /**
     * Fragment  事务管理
     *
     * @param layoutID
     * @param fragment
     * @return
     */
    protected FragmentTransaction fragmentReplace(int layoutID, Fragment fragment) {
        return mFragmentManager.beginTransaction().replace(layoutID, fragment);
    }

    protected FragmentTransaction fragmentAdd(int layoutID, Fragment fragment) {
        return mFragmentManager.beginTransaction().add(layoutID, fragment);
    }

    /**
     * 页面跳转
     */
    protected void jumpToActivity(Intent intent) {
        startActivity(intent);
//        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

    }

    protected void jumpToActivity(Class activity) {
        Intent intent = new Intent(this, activity);
        startActivity(intent);
//        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCompositeDisposable.size() > 0) {
            mCompositeDisposable.dispose();
        }

        /**
         * 将Presenter 持有的Activity引用 置空  解除订阅关系
         */
        if (mPresenter != null) {
            mPresenter.detachView();
        }

        /**
         * ButterKnife
         */
        mUnbinder.unbind();

        /**
         * 将当前Activity 从集合中删除掉
         */
        hideWaitingDialog();
        MainApp.getInstance().removeActivity(this);
    }

    protected abstract void initEventAndData();

    protected abstract T initPresenter();

    protected abstract int getLayout();
}
