package com.android.guoguang.lkmvpapp.base;


import com.android.guoguang.lkmvpapp.mvp.RxPresenter;
import com.android.guoguang.lkmvpapp.net.APIService;
import com.android.guoguang.lkmvpapp.net.RetrofitClient;

/**
 * 版权所有：中国农业银行软件开发中心
 * 系统名称：mmspc
 * 创建时间：2017/5/10
 * 作者：刘孟轩
 */

public class BaseRequest {
    //protected Retrofit mRetrofit = RetrofitService.getInstance().createRetrofit();
    protected RxPresenter mRxPresenter;
    protected APIService webRequest;

    public BaseRequest(RxPresenter mRxPresenter) {
        this.mRxPresenter = mRxPresenter;
        webRequest = RetrofitClient.getInstance().getApi();
    }
}
