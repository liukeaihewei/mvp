package com.android.guoguang.lkmvpapp.mvp.model;

import com.android.guoguang.lkmvpapp.base.BaseObjectBean;
import com.android.guoguang.lkmvpapp.base.BaseRequest;
import com.android.guoguang.lkmvpapp.bean.LoginBean;
import com.android.guoguang.lkmvpapp.mvp.RxPresenter;
import com.android.guoguang.lkmvpapp.mvp.contract.LoginContract;
import com.android.guoguang.lkmvpapp.net.OnNetRequestListener;
import com.android.guoguang.lkmvpapp.untils.rxuntils.RxScheduler;
import com.android.guoguang.lkmvpapp.untils.rxuntils.Subscribe2Help;

import io.reactivex.Observable;

/**
 * Created by liuke on 2018/12/19.
 */

public class LoginModel2 extends BaseRequest implements LoginContract.Model2 {
    public LoginModel2(RxPresenter mRxPresenter) {
        super(mRxPresenter);
    }

    @Override
    public void login(String username, String password, final OnNetRequestListener<BaseObjectBean<LoginBean>> listener) {
        /*listener.onStart();
        RetrofitClient.getInstance().getApi().login(username, password)
                .compose(RxScheduler.<BaseObjectBean<LoginBean>>Flo_io_main())
                .subscribe(new Consumer<BaseObjectBean<LoginBean>>() {
                    @Override
                    public void accept(BaseObjectBean<LoginBean> bean) throws Exception {
                        listener.onSuccess(bean);
                        listener.onFinish();
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        listener.onFailure(throwable);
                    }
                });*/
        //如果是异步操作就把返回的Disposable 取出 添加
        Observable<BaseObjectBean<LoginBean>> observable = webRequest.ologin(username, password);
        listener.onStart();
        observable.compose(RxScheduler.<BaseObjectBean<LoginBean>>Obs_io_main())

                .subscribe(new Subscribe2Help<BaseObjectBean<LoginBean>>(mRxPresenter) {
                    @Override
                    public void onNext(BaseObjectBean<LoginBean> value) {
                        listener.onSuccess(value);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        super.onError(throwable);
                        listener.onFailure(throwable);
                    }

                    @Override
                    public void onComplete() {
                        listener.onFinish();
                    }
                });
    }
}

