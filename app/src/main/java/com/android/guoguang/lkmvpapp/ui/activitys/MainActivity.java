package com.android.guoguang.lkmvpapp.ui.activitys;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.guoguang.lkmvpapp.R;
import com.android.guoguang.lkmvpapp.base.BaseActivity;
import com.android.guoguang.lkmvpapp.base.BaseObjectBean;
import com.android.guoguang.lkmvpapp.bean.LoginBean;
import com.android.guoguang.lkmvpapp.mvp.contract.LoginContract;
import com.android.guoguang.lkmvpapp.mvp.presenter.LoginPresenter;
import com.android.guoguang.lkmvpapp.ui.widgets.ProgressDialog;
import com.android.guoguang.lkmvpapp.untils.commonuntils.BaseUtils;
import com.android.guoguang.lkmvpapp.untils.commonuntils.SnackBarUtils;
import com.android.guoguang.lkmvpapp.untils.commonuntils.ToastUtils;
import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

/**
 * ui就是拿到已经直接可以显示的数据，而不是还要逻辑变换计算才能显示
 * 每天晚上回去做一点,关键要理解
 * MVP基础框架，第一版创建时间：2018.12.4
 * 到现在为止涵盖技术：mvp + rxjava2 + retrofit2 + autodispose
 * 做此目的就是快速开发，为了防止每次都去搭框架
 */
public class MainActivity extends BaseActivity<LoginPresenter> implements LoginContract.view{

    @BindView(R.id.iv_back_toolbar)
    ImageView ivBackToolbar;
    @BindView(R.id.tv_title_toolbar)
    TextView tvTitleToolbar;
    @BindView(R.id.appbar_layout_toolbar)
    AppBarLayout appbarLayoutToolbar;
    @BindView(R.id.et_username_login)
    TextInputEditText etUsernameLogin;
    @BindView(R.id.textlayout_username_login)
    TextInputLayout textlayoutUsernameLogin;
    @BindView(R.id.et_password_login)
    TextInputEditText etPasswordLogin;
    @BindView(R.id.textlayout_password_login)
    TextInputLayout textlayoutPasswordLogin;
    @BindView(R.id.btn_signin_login)
    Button btnSigninLogin;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initEventAndData() {
        requestPermissions(this);
        etUsernameLogin.setText(R.string.username);
        etPasswordLogin.setText(R.string.username);
    }

    private void requestPermissions(Activity activity) {
        Disposable permissionsDisposable = new RxPermissions(activity)
                .requestEach(Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(new Consumer<Permission>() {
                    @Override
                    public void accept(Permission permission) throws Exception {
                        if (permission.granted) {
                            // 用户已经同意该权限
                            ToastUtils.show("用户已经同意该权限");
                            // Timber.d("%s is granted.", permission.name);
                        } else if (permission.shouldShowRequestPermissionRationale) {
                            // 用户拒绝了该权限，没有选中『不再询问』（Never ask again）,那么下次再次启动时，还会提示请求权限的对话框
                            ToastUtils.show("用户拒绝开启" + permission.name + "读写权限,app无法使用");
                            //Timber.d("%s is denied. More info should be provided.", permission.name);
                        } else {
                            // 用户拒绝了该权限，并且选中『不再询问』
                            ToastUtils.show("用户拒绝了该权限，并且选中『不再询问』");
                            SnackBarUtils.makeShort(MainActivity.this.getWindow().getDecorView(), "读写权限被禁止,移步到应用管理允许权限").show("去设置", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    BaseUtils.getAppDetailSettingIntent(mContext, getPackageName());
                                }
                            });
                            //Timber.d("%s is denied.", permission.name);
                        }
                    }
                });
        mCompositeDisposable.add(permissionsDisposable);
    }

    /**
     * @return 帐号 liukeke
     */
    private String getUsername() {
        return etUsernameLogin.getText().toString().trim();
    }

    /**
     * @return 密码 liukeke
     */
    private String getPassword() {
        return etPasswordLogin.getText().toString().trim();
    }


    @OnClick(R.id.btn_signin_login)
    public void onViewClicked() {
        if (getUsername().isEmpty() || getPassword().isEmpty()) {
            Toast.makeText(this, "帐号密码不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        mPresenter.login(getUsername(), getPassword());
    }

    @Override
    protected LoginPresenter initPresenter() {
        return new LoginPresenter();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void showLoading() {
        ProgressDialog.getInstance().show(this);
    }

    @Override
    public void hideLoading() {
        ProgressDialog.getInstance().dismiss();
    }

    @Override
    public void showError(String msg) {

    }

    @Override
    public void onError(Throwable throwable) {

    }

    @Override
    public void showWaitingDialog(String tip) {

    }

    @Override
    public void hideWaitingDialog() {

    }

    @Override
    public void onSuccess(BaseObjectBean<LoginBean> bean) {
        Toast.makeText(this,"登录成功",Toast.LENGTH_SHORT).show();
    }
}
