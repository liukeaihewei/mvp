package com.android.guoguang.lkmvpapp.untils.rxuntils;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.guoguang.lkmvpapp.MainApp;
import com.android.guoguang.lkmvpapp.mvp.RxPresenter;
import com.android.guoguang.lkmvpapp.untils.commonuntils.NewWorkUtils;
import com.android.guoguang.lkmvpapp.untils.excpetion.ApiExcpetion;
import com.android.guoguang.lkmvpapp.untils.excpetion.NetExcpetion;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

/**
 * Created by sjl on 2017/5/2.
 */

public abstract class Subscribe2Help<T> implements Observer<T> {
    RxPresenter mRxPresenter;

    public Subscribe2Help(RxPresenter rxPresenter) {
        this.mRxPresenter = rxPresenter;
    }

    /**
     * 将请求添加到 CompositeDisposable 对象中 用于管理订阅的生命周期
     *
     * @param d
     */
    @Override
    public void onSubscribe(Disposable d) {
        mRxPresenter.addSubscribe(d);
    }

    /**
     * 异常处理
     *
     * @param throwable
     */
    @Override
    public void onError(Throwable throwable) {
        if (!NewWorkUtils.isNetworkConnected()) {
            Log.d("TAG", "无网络，请检查您的网络连接状态");
            Toast.makeText(MainApp.getInstance().getApplicationContext(), "无网络，请检查您的网络连接状态", Toast.LENGTH_SHORT).show();
        } else if (throwable instanceof ApiExcpetion) {
            Log.d("TAG", "" + throwable.getMessage() + "---" + throwable);
            Toast.makeText(MainApp.getInstance().getApplicationContext(), "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
        } else if (throwable instanceof ConnectException) {
            //Log.d("TAG", MainApp.getInstance().getApplicationContext().getString(R.string.connect_timeout) + "" + throwable);
            //Toast.makeText(MainApp.getInstance().getApplicationContext(), MainApp.getInstance().getApplicationContext().getString(R.string.connect_timeout) + "", Toast.LENGTH_SHORT).show();
        } else if (throwable instanceof SocketTimeoutException) {
            //Log.d("TAG", MainApp.getInstance().getApplicationContext().getString(R.string.socket_timeout) + "" + throwable);
            //Toast.makeText(MainApp.getInstance().getApplicationContext(), MainApp.getInstance().getApplicationContext().getString(R.string.socket_timeout) + "", Toast.LENGTH_SHORT).show();

        } else if (throwable instanceof HttpException) {
            if (((HttpException) throwable).code() == NetExcpetion.GATEWAY_TIMEOUT) {
                //Log.d("TAG", MainApp.getInstance().getApplicationContext().getString(R.string.gateway_timeout) + ((HttpException) throwable).code() + throwable.getMessage());
                //Toast.makeText(MainApp.getInstance().getApplicationContext(), MainApp.getInstance().getApplicationContext().getString(R.string.gateway_timeout) + "", Toast.LENGTH_SHORT).show();

            } else if (((HttpException) throwable).code() == NetExcpetion.UNAUTHORIZED) {
                //Log.d("TAG", MainApp.getInstance().getApplicationContext().getString(R.string.unauthorized) + "" + ((HttpException) throwable).code() + throwable.getMessage());
                //Toast.makeText(MainApp.getInstance().getApplicationContext(), MainApp.getInstance().getApplicationContext().getString(R.string.unauthorized), Toast.LENGTH_LONG).show();
                long systemCurrentTime = System.currentTimeMillis();
                Log.e("TAG","异常：systemCurrentTime==="+systemCurrentTime);
/*                long rmTokenTimeout = SpUtils.getInstance().getLong(Constants.RM_TOKEN_TIMEOUT, 0);
                Log.e("TAG","异常：rmTokenTimeout==="+rmTokenTimeout);
                if(systemCurrentTime > rmTokenTimeout){
                    SpUtils.getInstance().setInt(Constants.LOGIN_TYPE,0);
                    Log.e("TAG","异常：LOGIN_TYPE===0");
                }*/
                try {
                    Intent intent = new Intent();
                    intent.setAction("com.abchina.mmspc.activity.LoginActivity");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    MainApp.getInstance().getApplicationContext().startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }

            } else {
                //Log.d("TAG", MainApp.getInstance().getApplicationContext().getString(R.string.server_error) + "" + ((HttpException) throwable).code() + throwable.getMessage());
                //Toast.makeText(MainApp.getInstance().getApplicationContext(), MainApp.getInstance().getApplicationContext().getString(R.string.server_error) + "", Toast.LENGTH_SHORT).show();

            }
        } else {
            //Log.d("TAG", MainApp.getInstance().getApplicationContext().getString(R.string.unknown_error) + "" + throwable.getMessage());
            Toast.makeText(MainApp.getInstance().getApplicationContext(), "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onComplete() {
        return;
    }

}
